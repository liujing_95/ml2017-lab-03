import cv2
import numpy as np
from utils.tools import Tools
from feature import NPDFeature
from sklearn.model_selection import train_test_split
from ensemble import AdaBoostClassifier
from sklearn.metrics import classification_report

if __name__ == "__main__":
    # write your code here

    image_input_dir = './datasets/original'
    image_output_dir = './datasets/gray'
    features_save_path = './datasets/feature.npz'
    results_path = './report.txt'
    tools = Tools()

    # convert original image to 24 x 24 gray image.
    # tools.convert_rgb_to_gray_24_24(image_input_dir, image_output_dir)

    # extract features
    # image_path_list = tools.get_image_path_list(image_output_dir)
    # num_of_image = len(image_path_list)
    # n_pixels = 24 * 24
    # feature_dim = n_pixels * (n_pixels - 1) // 2
    # X = np.zeros((num_of_image, feature_dim), dtype=np.float32)
    # y = np.zeros((num_of_image, ), dtype=np.int32)
    # for i, image_path in enumerate(image_path_list):
    #     image = cv2.imread(image_path, 0)
    #     image = np.array(image)
    #     feature = NPDFeature(image).extract()
    #     if 'nonface' in image_path:
    #         y[i] = -1
    #     else:
    #         y[i] = 1
    #     X[i, :] = feature
    # np.savez(features_save_path, X, y)

    # load features
    npzfile = np.load(features_save_path)
    X = npzfile['arr_0']
    y = npzfile['arr_1']

    # Split the dataset into training set and validation set
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42, shuffle=True)
    adaboost_classifier = AdaBoostClassifier(nrof_classifier = 20)
    # Train the model
    adaboost_classifier.fit(X_train, y_train)
    y_predict = adaboost_classifier.predict(X_test)
    target_names = ['non_face', 'face']
    accuracy = np.mean(y_predict == y_test)
    print(accuracy)
    results = classification_report(y_test, y_predict, target_names = target_names)
    with open(results_path, 'w+') as f:
        f.write(results)
    print(results)