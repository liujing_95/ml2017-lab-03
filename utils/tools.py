import os
import cv2

class Tools():

    def convert_rgb_to_gray_24_24(self, input_dir, output_dir):
        image_path_list = self.get_image_path_list(input_dir)
        for image_path in image_path_list:
            image = cv2.imread(image_path, 0)
            image = cv2.resize(image, (24, 24))
            output_path = image_path.replace(input_dir, output_dir)
            output_path_dir = os.path.dirname(output_path)
            if not os.path.exists(output_path_dir):
                os.makedirs(output_path_dir)
            cv2.imwrite(output_path, image)

    def get_image_path_list(self, folder):
        root_list = os.listdir(folder)
        image_path_list = []
        for f in root_list:
            f_path = os.path.join(folder, f)
            if os.path.isdir(f_path):
                sub_list = self.get_image_path_list(f_path)
                image_path_list += sub_list
            else:
                file_name, file_ext = os.path.splitext(f)
                if '.png' not in file_ext and '.jpg' not in file_ext and '.bmp' not in file_ext and '.jpeg' not in file_ext and '.PNG' not in file_ext and '.JPG' not in file_ext and '.BMP' not in file_ext and '.JPEG' not in file_ext:
                    continue
                image_path_list.append(f_path)
        return image_path_list