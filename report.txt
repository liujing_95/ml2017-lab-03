             precision    recall  f1-score   support

   non_face       0.95      0.96      0.96       165
       face       0.96      0.95      0.96       165

avg / total       0.96      0.96      0.96       330
